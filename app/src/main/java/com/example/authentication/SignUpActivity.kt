package com.example.authentication

import android.os.Bundle
import android.text.TextUtils
import android.util.Log.d
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_sign_up.*

class SignUpActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)
        init()
    }

    private fun init() {
        auth = Firebase.auth
        signUpButton1.setOnClickListener {
            signup()

        }
    }


    private fun signup() {
        val email = emailEditText.text.toString()
        val password = passwordEditText.text.toString()
        val repeatPassword = repeatPasswordEditText.text.toString()


        if (email.isNotEmpty() && password.isNotEmpty() && repeatPassword.isNotEmpty()) {
            if (password == repeatPassword) {
                if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    progressBar.visibility = View.VISIBLE
                    signUpButton1.isClickable = false
                    auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this) { task ->
                            progressBar.visibility = View.GONE
                            signUpButton1.isClickable = true
                            if (task.isSuccessful) {
                                // Sign in success, update UI with the signed-in user's information
                                d("signUp", "createUserWithEmail:success")
                                Toast.makeText(
                                    baseContext, "Sign Up is Success!",
                                    Toast.LENGTH_SHORT
                                ).show()
                                val user = auth.currentUser
                            } else {
                                // If sign in fails, display a message to the user.
                                d("signUp", "createUserWithEmail:failure", task.exception)
                                Toast.makeText(
                                    baseContext, "Authentication failed.",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                } else {
                    Toast.makeText(this, "Email format is not Correct!", Toast.LENGTH_SHORT).show()
                }
            }else{
                Toast.makeText(this,"Passwords don't match",Toast.LENGTH_LONG).show()
            }
        }else{
            Toast.makeText(this,"Please fill all the fields",Toast.LENGTH_SHORT).show()
        }
    }
}