package com.example.authentication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.util.Log.d
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_authentication.*
import kotlinx.android.synthetic.main.activity_sign_in.*
import kotlinx.android.synthetic.main.activity_sign_in.signInButton as signInButton1

class SignInActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        init()
    }
    private fun init(){
        auth = Firebase.auth
        signInButton.setOnClickListener {
            signIn()
        }
    }
    private fun signIn(){
        val email:String = emailEditText.text.toString()
        val password:String = passwordEditText.text.toString()
        if(email.isNotEmpty() && password.isNotEmpty()) {
            progressBar.visibility= View.VISIBLE
            signInButton.isClickable=false
            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    progressBar.visibility= View.GONE
                    signInButton.isClickable=true
                    if (task.isSuccessful) {
                        d("Login", "signInWithEmail:success")
                        val user = auth.currentUser
                        Toast.makeText(baseContext, "Sign In is Success!", Toast.LENGTH_SHORT).show()
                    } else {
                        Log.w("Login", "signInWithEmail:failure", task.exception)
                        Toast.makeText(baseContext, "Authentication failed."+" "+task.exception?.message, Toast.LENGTH_SHORT).show()
                    }
                }

        } else {
            Toast.makeText(this, "Please fill all the fields", Toast.LENGTH_SHORT).show()
        }
    }
}